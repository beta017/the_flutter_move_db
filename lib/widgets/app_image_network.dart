import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_move_db/services/networking.dart';
import 'package:flutter_move_db/widgets/custom_loading_spin_kit_ring.dart';
import 'package:sizer/sizer.dart';

class AppImageNetwork extends StatefulWidget {
  final String imageUrl;
  final Color themeColor;
  final double? height;
  final double? width;
  final BoxFit? fit;

  const AppImageNetwork({
    Key? key,
    required this.imageUrl,
    required this.themeColor,
    this.height,
    this.width,
    this.fit,
  }) : super(key: key);

  @override
  State<AppImageNetwork> createState() => _AppImageNetworkState();
}

class _AppImageNetworkState extends State<AppImageNetwork> {
  late final NetworkHelper _network;
  Uint8List? _image;

  @override
  void initState() {
    super.initState();
    _network = NetworkHelper(Uri.parse(widget.imageUrl));
    loadData();
  }

  Future<void> loadData() async {
    _image = await _network.getImage();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (_image == null) {
      // return CustomLoadingSpinKitRing(loadingColor: widget.themeColor);
      return Container(
        height: 20.h,
        child: CustomLoadingSpinKitRing(loadingColor: widget.themeColor),
      );
    } else {
      return Image.memory(
        _image!,
        height: widget.height,
        width: widget.width,
        fit: widget.fit,
      );
    }
  }
}
