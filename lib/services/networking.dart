import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:dio/adapter.dart';
import 'package:flutter_move_db/secret/proxy_settings.dart';

class NetworkHelper {
  final Uri url;
  late final HttpClient _client;
  NetworkHelper(this.url) {
    _client = HttpClient();
    _client.findProxy = (uri) {
      return "PROXY $proxyIP:$proxyPort;";
    };
    _client.addProxyCredentials(proxyIP, proxyPort, "",
        HttpClientBasicCredentials(proxyLogin, proxyPassword));
  }

  Future<dynamic> getData() async {
    final request = await _client.getUrl(url);
    final response = await request.close();
    final json = await response
        .transform(utf8.decoder)
        .toList()
        .then((v) => v.join())
        .then((v) => jsonDecode(v) as Map<String, dynamic>);

    if (response.statusCode == 200) {
      return json;
    } else {
      print(response.statusCode);
    }
  }

  Future<Uint8List> getImage() async {
    final dio = Dio();
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (client) {
      client.findProxy = (uri) {
        return "PROXY $proxyIP:$proxyPort";
      };
      client.addProxyCredentials(proxyIP, proxyPort, "",
          HttpClientBasicCredentials(proxyLogin, proxyPassword));
    };
    final result = await dio.getUri(url,
        options: Options(responseType: ResponseType.bytes));
    return result.data;
  }
}
